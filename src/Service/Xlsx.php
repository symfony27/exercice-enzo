<?php

namespace App\Service;

use App\Entity\Post;
use Doctrine\Persistence\ManagerRegistry;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use Symfony\Component\HttpKernel\KernelInterface;

class Xlsx
{
    /** @var ManagerRegistry  */
    private $managerRegistry;

    /** @var KernelInterface  */
    private $kernel;

    /**
     * @param ManagerRegistry $managerRegistry
     * @param KernelInterface $kernel
     */
    public function __construct(ManagerRegistry $managerRegistry, KernelInterface $kernel)
    {
        $this->managerRegistry = $managerRegistry;
        $this->kernel = $kernel;
    }

    /**
     * @return string
     */
    public function export(): string
    {
        $repository = $this->managerRegistry->getRepository(Post::class);
        $posts = $repository->findAll();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($this->getFormatArray($posts));
        $writer = new Writer($spreadsheet);
        $filePath = $this->getFilePath();
        try {
            $writer->save($filePath);
            return $filePath;
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @param array<Post> $posts
     * @return array
     */
    public function getFormatArray(array $posts): array
    {
        $formatArray = [];
        foreach ($posts as $post) {
            $formatArray[] = [$post->getId(), $post->getTitle(), $post->getContent()];
        }
        return $formatArray;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        $publicDir = $this->kernel->getProjectDir() . '/public';
        return $publicDir . '/' . uniqid() . '.xlsx';
    }
}