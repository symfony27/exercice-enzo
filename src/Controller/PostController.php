<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\Type\PostType;
use App\Service\Xlsx;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/posts")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="posts")
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function showAll(ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getRepository(Post::class);
        $posts = array_reverse($entityManager->findAll());

        return $this->render('posts/showAll.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function createPost(Request $request, ManagerRegistry $doctrine): Response
    {
        $post = new Post();
        $entityManager = $doctrine->getManager();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($post);
            $entityManager->flush();
            return $this->redirectToRoute('posts');
        }

        return $this->render('posts/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param int $id
     * @return Response
     */
    public function editPost(Request $request, ManagerRegistry $doctrine, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $post = $entityManager->getRepository(Post::class)->find($id);
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($post);
            $entityManager->flush();
            return $this->redirectToRoute('posts');
        }

        return $this->render('posts/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param int $id
     * @return Response
     */
    public function deletePost(Request $request, ManagerRegistry $doctrine, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $post = $entityManager->getRepository(Post::class)->find($id);
        $entityManager->remove($post);
        $entityManager->flush();

        return $this->redirectToRoute('posts');
    }

    /**
     * @Route("/export/xlsx", name="export-xlsx")
     * @param Xlsx $xlsx
     * @return Response
     */
    public function exportXlsx(Xlsx $xlsx): Response
    {
        $filePath = $xlsx->export();
        $response = $this->file($filePath, 'export.xlsx');
        $response->deleteFileAfterSend();
        return $response;
    }
}