<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param ManagerRegistry $doctrine
     * @return Response
     */
    public function index(ManagerRegistry $doctrine): Response
    {
        /** @var PostRepository $entityManager */
        $entityManager = $doctrine->getRepository(Post::class);
        $lastPost = $entityManager->findLast();

        return $this->render('index.html.twig', ['lastPost' => $lastPost]);
    }
}