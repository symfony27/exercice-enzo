# Exercice Enzo

## Installation des dépendances

**L'application peut être déposée sur une VM ou lancée en local**  
  
**Nécéssite d'installer composer, nodejs, npm et yarn sur le poste ou sur la VM**  
  
**PHP 7.4 ou plus**

Exécuter à la racine du projet :

```
composer install
npm install
yarn watch
```

## Paramétrage de la base de données

**Il faut modifier le contenu du fichier .env à la racine du projet :**

Commenter la ligne postgre et décommenter mysql  
Entrer les informations de la base de données (utilisateur, mot de passe, hôte et nom de la base)

```
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"
# DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=14&charset=utf8"
```

**Créer la base de données :**

```
php bin/console doctrine:database:create
php bin/console make:migration
php bin/console doctrine:migrations:migrate
```

**En cas d'erreur lors de la migration, supprimer le fichier migrations/Version20220212223556.php et relancer la commande**

## Lancer le serveur

Si l'application est installée sur une VM, configurer apache pour qu'il pointe dans le dossier public  
  
Si l'application est lancée en local :
```
symfony server:start
```

Se rendre sur http://localhost:8000

## Lancer l'application avec docker

**Installer docker et docker-compose sur le poste ou la VM**  
  
**Attention, si docker est installé sur le poste, les VMs ne se lanceront plus.**  
**Il faudra changer une configuration pour qu'elles fonctionnent à nouveau**
  
A la racine du projet :
```
docker-compose up
```

Se rendre sur http://localhost:8000

## Easyadmin

Easyadmin est installé sur l'application :  
  
http://localhost/admin
