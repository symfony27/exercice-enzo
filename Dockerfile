FROM php:8.1-apache
RUN apt-get update
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions gd pdo_pgsql bcmath zip intl opcache
WORKDIR /var/www/app
RUN a2enmod rewrite